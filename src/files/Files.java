package files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileOutputStream;

public class Files {
    public static void main(String[] args) throws FileNotFoundException, IOException {

        //создаем каталог dir
        File dir = new File(".");

        //чтобы посмотреть содержимое каталога команда list (она возвращает массив строк)
        String [] names = dir.list();
        for(int i=0; i<names.length; i++) {

            System.out.println(names[i]);
        }
        //создаем объект с файлом и считаем его длину
        File file = new File("/Users/user/IdeaProjects/Files/src/JAVA Ext program for Dnipro.docx");
        System.out.println(file.length());

        FileInputStream inputStream = new FileInputStream(file);

       //available - проверить сколько байт доступно для чтения
        int length = inputStream.available();
        System.out.println(length);

        byte [] data = new byte[length];
        inputStream.read(data);

//        for (int i=0; i<data.length; i++) {
//            System.out.println(data[i]);
//        }

        String text = new String(data, "utf8");
        System.out.println(text);

        //Если не будет атрибута "append: true", то содержание файла перезапишится
        FileOutputStream outputStream = new FileOutputStream(file, true);
        String newText = "Hello world files!!!  ";
        byte[] newTextBytes = newText.getBytes();
        outputStream.write(newTextBytes);
        outputStream.close();
    }
}
